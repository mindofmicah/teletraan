<?php

namespace MindOfMicah\Teletraan;

use Illuminate\Database\MigrationServiceProvider;

class ServiceProvider extends MigrationServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config.php' => config_path('teletraan.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        parent::register();

        $this->app->singleton('command.console.make', function ($app) {
            return new ConsoleMakeCommand($app['files']);
        });

        $this->mergeConfigFrom(
            __DIR__ . '/../config.php', 'teletraan'
        );
    }
}
