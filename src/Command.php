<?php
namespace MindOfMicah\Teletraan;

use Illuminate\Console\Command as LaravelCommand;

class Command extends LaravelCommand
{
    protected $queued_output = [];

    /**
     * Store the output to be displayed all at once later
     *
     * @param string $display_type
     * @param array  ...$output
     */
    protected function queueOutput(string $display_type, ...$output)
    {
        $this->queued_output[] = compact('display_type', 'output');
    }

    /**
     * Spit out the output that has been saved until later
     */
    protected function printQueuedOutput()
    {
        array_walk($this->queued_output, function (array $output) {
            call_user_func_array([$this, $output['display_type']], $output['output']);
        });
    }

}
