<?php

namespace MindOfMicah\Teletraan;

use Illuminate\Console\Scheduling\Schedule as LaravelSchedule;

class Schedule extends LaravelSchedule
{
    private $default_email_address;

    /**
     * @param string $email_address
     */
    public function setDefaultEmailAddress(string $email_address)
    {
        $this->default_email_address = $email_address;
    }

    /**
     * @param string $command_name
     * @param string $subject
     *
     * @return \Illuminate\Console\Scheduling\Event
     */
    public function emailedCommand(string $command_name, string $subject = '')
    {
        preg_match_all('/\b[\w]/', $command_name . ' ' . $subject, $matches);

        return $this->command($command_name)
            ->description($subject ?: $command_name)
            ->sendOutputTo('/tmp/' . implode('', $matches[0]))
            ->emailWrittenOutputTo($this->default_email_address);
    }
}
