<?php
namespace MindOfMicah\Teletraan;

use Illuminate\Console\Scheduling\Schedule as LaravelSchedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected function defineConsoleSchedule()
    {
        $this->app->singleton(LaravelSchedule::class, function () {
            $schedule = new Schedule;

            if ($default_email = config('teletraan.email')) {
                $schedule->setDefaultEmailAddress($default_email);

            }

            return $schedule;
        });

        $schedule = $this->app->make(LaravelSchedule::class);

        $this->schedule($schedule);
    }
}
