<?php
namespace MindOfMicah\Teletraan;

use Illuminate\Console\Command as LaravelCommand;
use Illuminate\Foundation\Console\ConsoleMakeCommand as BaseCommand;

class ConsoleMakeCommand extends BaseCommand
{
    /**
     * Replace the class name for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     *
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        return str_replace(LaravelCommand::class, Command::class, parent::replaceClass($stub, $name));
    }
}
